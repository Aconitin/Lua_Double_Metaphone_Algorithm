Double Metaphone algorithm, implemented in Lua!

To use, either call "double_metaphone.transscript(string)" from inside a script

Or call "lua52.exe double_metaphone.lua string" from a windows command line

For more information, please visit SvenRaveny.net/articles/double_metaphone.html

If you find bugs, please let me know via twitter @Aconitin_

Have a nice day!