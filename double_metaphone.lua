--> Double Metaphone algorithm, implemented in Lua by Sven Raveny!
--> To use, either call "double_metaphone.transscript(string)" from inside a script
--> Or call "lua52.exe double_metaphone.lua string" from a windows command line
--> For more information, please visit SvenRaveny.net/articles/double_metaphone.html
--> If you find bugs, please let me know via twitter @Aconitin_
--> Have a nice day!

double_metaphone = {}
double_metaphone.params = {maxOutputLetters = 0 --> Default for the double Metaphone algorithm is 4. Enter 0 for infinite!
							  }

function double_metaphone.string_at(str, start, length, list)
	if (start < 1) or (start > str:len()) then
		return false
	end
	local i, len = 0, double_metaphone.ll(list)
	while i<=len do
		i = i + 1
		if list[i] == string.sub(str, start, start + length - 1) then
			return true
		end
	end
	return false
end

function double_metaphone.ll(list)
	local count = 0
	for i,v in pairs(list) do
		count = count + 1
	end
	return count
end

function double_metaphone.is_vowel(str, pos)
	local vowels = {A = true, E = true, I = true, O = true, U = true, Y = true}
	return vowels[str:sub(pos, pos)]
end

function double_metaphone.slavo_Germanic(str)
	if string.find(str, "W") or string.find(str, "K") or string.find(str, "CZ") or string.find(str, "WITZ") then
		return true
	end
	return false
end
  
function double_metaphone.transscript(str)
	local primary = ""
	local secondary = ""
	local current = 1
	local length = str:len()
	local last = length - 1
	local original = str .. "     "
	original = original:upper()
	local maxOutputLetters = double_metaphone.params.maxOutputLetters
	if maxOutputLetters == 0 then
		maxOutputLetters = str:len() * 1337
	end
	if double_metaphone.string_at(original, 1, 2, {"GN", "KN", "PN", "WR", "PS"}) then
		current = current + 1
	end
	if (original:sub(1, 1) == 'X') then
		primary = primary .. "S"  
		secondary = secondary .. "S"  
		current = current + 1
	end

    while (primary:len() < maxOutputLetters) or (secondary:len() < maxOutputLetters) do
		local oldCurrent = current
        if current > length then
            break
        end
        local vowels = {A = true, E = true, I = true, O = true, U = true, Y = true}
        if vowels[original:sub(current, current)] then
            if current == 1 then
                primary = primary .. "A"
                secondary = secondary .. "A"
            end
            current = current + 1
			goto theEnd
        elseif original:sub(current, current) == "B" then
            primary = primary .. "P"
            secondary = secondary .. "P"
            if original:sub(current + 1, current + 1) == "B" then
                current = current + 2
            else
                current = current + 1
				goto theEnd
            end
        elseif original:sub(current, current) == "�" then
            primary = primary .. "S"
            secondary = secondary .. "S"        
            current = current + 1
			goto theEnd
		elseif original:sub(current, current) == "�" then
			primary = primary .. "S"
            secondary = secondary .. "S"        
            current = current + 1
			goto theEnd
		elseif original:sub(current, current) == "C" then
			if ((current > 1) and (double_metaphone.is_vowel(original, current - 2) == false) and double_metaphone.string_at(original, (current - 1), 3, {"ACH"}) and ((original:sub(current + 2) ~= "I") and ((original:sub(current + 2) ~= "E") or double_metaphone.string_at(original, (current - 2), 6, {"BACHER", "MACHER"})))) then
				primary = primary .. "K"
				secondary = secondary .. "K"        
				current = current + 2
				goto theEnd
			end
			if ((current == 1) and double_metaphone.string_at(original, current, 6, {"CAESAR"})) then
				primary = primary .. "S"
				secondary = secondary .. "S"   
				current = current + 2
				goto theEnd
			end		
			if (double_metaphone.string_at(original, current, 4, {"CHIA"})) then
				primary = primary .. "K"
				secondary = secondary .. "K"   
				current = current + 2
				goto theEnd
			end
			if (double_metaphone.string_at(original, current, 2, {"CH"})) then
				if ((current > 1) and double_metaphone.string_at(original, current, 4, {"CHAE"})) then
					primary = primary .. "K"
					secondary = secondary .. "X"   
					current = current + 2
					goto theEnd
				end
				if ((current == 1) and (double_metaphone.string_at(original, (current + 1), 5, {"HARAC", "HARIS"}) or double_metaphone.string_at(original, (current + 1), 3, {"HOR", "HYM", "HIA", "HEM"}))	and not double_metaphone.string_at(original, 1, 5, {"CHORE"})) then
					primary = primary .. "K"
					secondary = secondary .. "K"
					current = current + 2
					goto theEnd
				end
				if ((double_metaphone.string_at(original, 1, 4, {"VAN ", "VON "}) or double_metaphone.string_at(original, 1, 3, {"SCH"}))
				or double_metaphone.string_at(original, (current - 2), 6, {"ORCHES", "ARCHIT", "ORCHID"})
				or double_metaphone.string_at(original, (current + 2), 1, {"T", "S"})
				or ((double_metaphone.string_at(original, (current - 1), 1, {"A", "O", "U", "E"}) or (current == 1))
				and double_metaphone.string_at(original, (current + 2), 1, {"L", "R", "N", "M", "B", "H", "F", "V", "W", " "}))) then
					primary = primary .. "K"
					secondary = secondary .. "K"
				else
					if (current > 1) then
						if (double_metaphone.string_at(original, 1, 2, {"MC"})) then
							primary = primary .. "K"
							secondary = secondary .. "K"
						else
							primary = primary .. "X"
							secondary = secondary .. "K"
						end
					else
						primary = primary .. "X"
						secondary = secondary .. "X"
					end
				end
				current = current + 2
				goto theEnd
			end
			if (double_metaphone.string_at(original, current, 2, {"CZ"}) and not double_metaphone.string_at(original, (current - 2), 4, {"WICZ"})) then
				primary = primary .. "S"
				secondary = secondary .. "X"
				current = current + 2
				goto theEnd
			end
			if (double_metaphone.string_at(original, current, 3, {"CIA"})) then
				primary = primary .. "X"
				secondary = secondary .. "X"
				current = current + 3
				goto theEnd
			end
			if (double_metaphone.string_at(original, current, 2, {"CC"}) and not ((current == 2) and (original:sub(1, 1) == 'M'))) then
				if (double_metaphone.string_at(original, (current + 2), 1, {"I", "E", "H"}) and not double_metaphone.string_at(original, (current + 2), 2, {"HU"})) then
					if (((current == 2) and (original:sub(current - 1, current -1) == "A")) or double_metaphone.string_at(original, (current - 1), 5, {"UCCEE", "UCCES"})) then
						primary = primary .. "KS"
						secondary = secondary .. "KS"
					else
						primary = primary .. "X"
						secondary = secondary .. "X"
					end
					current = current + 3
					goto theEnd
				else
					primary = primary .. "K"
					secondary = secondary .. "K"
					current = current + 2
					goto theEnd
				end
			end
			if (double_metaphone.string_at(original, current, 2, {"CK", "CG", "CQ"})) then
				primary = primary .. "K"
				secondary = secondary .. "K"
				current = current + 2
				goto theEnd
			end
			if (double_metaphone.string_at(original, current, 2, {"CI", "CE", "CY"})) then
				if (double_metaphone.string_at(original, current, 3, {"CIO", "CIE", "CIA"})) then
					primary = primary .. "S"
					secondary = secondary .. "X"
				else
					primary = primary .. "S"
					secondary = secondary .. "S"
				end
				current = current + 2
				goto theEnd
			end
			primary = primary .. "K"
			secondary = secondary .. "K"
			if (double_metaphone.string_at(original, (current + 1), 1, {" C", " Q", " G"})) then
				current = current + 3
			elseif (double_metaphone.string_at(original, (current + 1), 1, {"C", "K", "Q"}) and not double_metaphone.string_at(original, (current + 1), 2, {"CE", "CI"})) then
				current = current + 2
			else
				current = current + 1
			end
			goto theEnd
		elseif original:sub(current, current) == "D" then
			if (double_metaphone.string_at(original, current, 2, {"DG"})) then
				if (double_metaphone.string_at(original, (current + 2), 1, {"I", "E", "Y"})) then
					primary = primary .. "J"
					secondary = secondary .. "J"
					current = current + 3
					goto theEnd
				else
					primary = primary .. "TK"
					secondary = secondary .. "TK"
					current = current + 2
					goto theEnd
				end
			end
			if (double_metaphone.string_at(original, current, 2, {"DT", "DD"})) then
				primary = primary .. "T"
				secondary = secondary .. "T"
				current = current + 2
				goto theEnd
			end
			primary = primary .. "T"
			secondary = secondary .. "T"
			current = current + 1
			goto theEnd
		elseif original:sub(current, current) == "F" then
			if (original:sub(current + 1, current + 1) == "F") then
				current = current + 2
			else
				current = current + 1
			end
			primary = primary .. "F"
			secondary = secondary .. "F"
			goto theEnd
		elseif original:sub(current, current) == "G" then
			if (original:sub(current + 1, current + 1) == "H") then
				if ((current > 1) and not double_metaphone.is_vowel(original, current - 1)) then
					primary = primary .. "K"
					secondary = secondary .. "K"
					current = current + 2
					goto theEnd
				end
				if (current < 4) then
					if (current == 1) then
						if (original:sub(current + 2, current + 2) == "I") then
							primary = primary .. "J"
							secondary = secondary .. "J"
						else
							primary = primary .. "K"
							secondary = secondary .. "K"
						end
						current = current + 2
						goto theEnd
					end
				end
				if (((current > 2) and double_metaphone.string_at(original, (current - 2), 1, {"B", "H", "D"}))
				or ((current > 3) and double_metaphone.string_at(original, (current - 3), 1, {"B", "H", "D"}))
				or ((current > 4) and double_metaphone.string_at(original, (current - 4), 1, {"B", "H"}))) then
					current = current + 2
					goto theEnd
				else
					if ((current > 3) and (original:sub(current - 1, current - 1) == "U") and double_metaphone.string_at(original, (current - 3), 1, {"C", "G", "L", "R", "T"})) then
						primary = primary .. "F"
						secondary = secondary .. "F"
					elseif ((current > 1) and original:sub(current - 1, current - 1) ~= "I") then
						primary = primary .. "K"
						secondary = secondary .. "K"
					end
					current = current + 2
					goto theEnd
				end
			end
			if (original:sub(current + 1, current + 1) == "N") then
				if ((current == 2) and double_metaphone.is_vowel(original, 1) and not double_metaphone.slavo_Germanic(original)) then
					primary = primary .. "KN"
					secondary = secondary .. "N"
				else
					if (not double_metaphone.string_at(original, (current + 2), 1, {"EY"}) and (original:sub(current + 1, current + 1) ~= "Y") and not double_metaphone.slavo_Germanic(original)) then
						primary = primary .. "N"
						secondary = secondary .. "KN"
					else
						primary = primary .. "KN"
						secondary = secondary .. "KN"
					end
				end
				current = current + 2
				goto theEnd
			end
			if (double_metaphone.string_at(original, (current + 1), 2, {"LI"}) and not double_metaphone.slavo_Germanic(original)) then
				primary = primary .. "KL"
				secondary = secondary .. "L"
				current = current + 2
				goto theEnd
			end
			if ((current == 1)
			and ((original:sub(current + 1, current + 1) == "Y") or double_metaphone.string_at(original, (current + 1), 2, {"ES", "EP", "EB", "EL", "EY", "IB", "IL", "IN", "IE", "EI", "ER"}))) then
				primary = primary .. "K"
				secondary = secondary .. "J"
				current = current + 2
				goto theEnd
			end
			if ((double_metaphone.string_at(original, (current + 1), 2, {"ER"}) or (original:sub(current + 1, current + 1) == "Y")) and not double_metaphone.string_at(original, 1, 6, {"DANGER", "RANGER", "MANGER"})
				and not double_metaphone.string_at(original, (current - 1), 1, {"E", "I"}) and not double_metaphone.string_at(original, (current - 1), 3, {"RGY", "OGY"})) then
				primary = primary .. "K"
				secondary = secondary .. "J"
				current = current + 2
				goto theEnd
			end
			if (double_metaphone.string_at(original, (current + 1), 1, {"E", "I", "Y"}) or double_metaphone.string_at(original, (current - 1), 4, {"AGGI", "OGGI"})) then
				if ((double_metaphone.string_at(original, 1, 4, {"VAN ", "VON "}) or double_metaphone.string_at(original, 1, 3, {"SCH"})) or double_metaphone.string_at(original, (current + 1), 2, {"ET"})) then
					primary = primary .. "K"
					secondary = secondary .. "K"
				else
					if (double_metaphone.string_at(original, (current + 1), 4, {"IER "})) then
						primary = primary .. "J"
						secondary = secondary .. "J"
					else
						primary = primary .. "J"
						secondary = secondary .. "K"
					end
				end
				current = current + 2
				goto theEnd
			end
			if (original:sub(current + 1, current + 1) == "G") then
				current = current + 2
			else
				current = current + 1
			end
			primary = primary .. "K"
			secondary = secondary .. "K"
			goto theEnd
		elseif original:sub(current, current) == "H" then
			if (((current == 1) or double_metaphone.is_vowel(original, current - 1)) and double_metaphone.is_vowel(original, current + 1)) then
				primary = primary .. "H"
				secondary = secondary .. "H"
				current = current + 2
			else
				current = current + 1
			end
			goto theEnd
		elseif original:sub(current, current) == "J" then
			if (double_metaphone.string_at(original, current, 4, {"JOSE"}) or double_metaphone.string_at(original, 1, 4, {"SAN "})) then
				if (((current == 1) and (original:sub(current + 4, current + 4) == " ")) or double_metaphone.string_at(original, 1, 4, {"SAN "})) then
					primary = primary .. "H"
					secondary = secondary .. "H"
				else
					primary = primary .. "J"
					secondary = secondary .. "H"
				end
				current = current + 1
				goto theEnd
			end
			if ((current == 1) and not double_metaphone.string_at(original, current, 4, {"JOSE"})) then
				primary = primary .. "J"
				secondary = secondary .. "A"
			else
				if double_metaphone.is_vowel(original, current - 1) and not double_metaphone.slavo_Germanic(original) and ((original:sub(current + 1, current + 1) == "A") or (original:sub(current + 1, current + 1) == "O")) then
					primary = primary .. "J"
					secondary = secondary .. "H"
				elseif (current == last) then
					primary = primary .. "J"
					secondary = secondary .. " "
				elseif (not double_metaphone.string_at(original, (current + 1), 1, {"L", "T", "K", "S", "N", "M", "B", "Z"}) and not double_metaphone.string_at(original, (current - 1), 1, {"S", "K", "L"})) then
					primary = primary .. "J"
					secondary = secondary .. "J"
				end
			end
			if (original:sub(current + 1, current + 1) == "J") then
				current = current + 2
			else
				current = current + 1
			end
			goto theEnd
		elseif original:sub(current, current) == "K" then
			if (original:sub(current + 1, current + 1) == "K") then
				current = current + 2
			else
				current = current + 1
			end
			primary = primary .. "K"
			secondary = secondary .. "K"
			goto theEnd
		elseif original:sub(current, current) == "L" then
			if (original:sub(current + 1, current + 1) == "L") then
				if (((current == (length - 3)) and double_metaphone.string_at(original, (current - 1), 4, {"ILLO", "ILLA", "ALLE"}))
					or ((double_metaphone.string_at(original, (last - 1), 2, {"AS", "OS"}) or double_metaphone.string_at(original, last, 1, {"A", "O"})) and double_metaphone.string_at(original, (current - 1), 4, {"ALLE"}))) then
					primary = primary .. "L"
					secondary = secondary .. " "
					current = current + 2
					goto theEnd
				end
				current = current + 2
			else
				current = current + 1
			end
			primary = primary .. "L"
			secondary = secondary .. "L"
			goto theEnd
		elseif original:sub(current, current) == "M" then
			if ((double_metaphone.string_at(original, (current - 1), 3, {"UMB"}) and (((current + 1) == last) or double_metaphone.string_at(original, (current + 2), 2, {"ER"})))
				or (original:sub(current + 1, current + 1) == "M")) then
				current = current + 2
			else
				current = current + 1
			end
			primary = primary .. "M"
			secondary = secondary .. "M"
			goto theEnd
		elseif original:sub(current, current) == "N" then
			if (original:sub(current + 1, current + 1) == "N") then
				current = current + 2
			else
				current = current + 1
			end
			primary = primary .. "N"
			secondary = secondary .. "N"
			goto theEnd
		elseif original:sub(current, current) == "�" then
			current = current + 1
			primary = primary .. "N"
			secondary = secondary .. "N"
			goto theEnd
		elseif original:sub(current, current) == "�" then
			current = current + 1
			primary = primary .. "N"
			secondary = secondary .. "N"
			goto theEnd
		elseif original:sub(current, current) == "P" then
			if (original:sub(current + 1, current + 1) == "H") then
				primary = primary .. "F"
				secondary = secondary .. "F"
				current = current + 2
				goto theEnd
			end
			if (double_metaphone.string_at(original, (current + 1), 1, {"P", "B"})) then
				current = current + 2
			else
				current = current + 1
			end
			primary = primary .. "P"
			secondary = secondary .. "P"
			goto theEnd
		elseif original:sub(current, current) == "Q" then
			if (original:sub(current + 1, current + 1) == "Q") then
				current = current + 2
			else
				current = current + 1
			end
			primary = primary .. "K"
			secondary = secondary .. "K"
			goto theEnd
		elseif original:sub(current, current) == "R" then
			if ((current == last) and not double_metaphone.slavo_Germanic(original) and double_metaphone.string_at(original, (current - 2), 2, {"IE"}) and not double_metaphone.string_at(original, (current - 4), 2, {"ME", "MA"})) then
				primary = primary .. ""
				secondary = secondary .. "R"
			else
				primary = primary .. "R"
				secondary = secondary .. "R"
			end
			if (original:sub(current + 1, current + 1) == "R") then
				current = current + 2
			else
				current = current + 1
			end
			goto theEnd
		elseif original:sub(current, current) == "S" then
			if (double_metaphone.string_at(original, (current - 1), 3, {"ISL", "YSL"})) then
				current = current + 1
				goto theEnd
			end
			if ((current == 1) and double_metaphone.string_at(original, current, 5, {"SUGAR"})) then
				primary = primary .. "X"
				secondary = secondary .. "S"
				current = current + 1
				goto theEnd
			end
			if (double_metaphone.string_at(original, current, 2, {"SH"})) then
				if (double_metaphone.string_at(original, (current + 1), 4, {"HEIM", "HOEK", "HOLM", "HOLZ"})) then
					primary = primary .. "S"
					secondary = secondary .. "S"
				else
					primary = primary .. "X"
					secondary = secondary .. "X"
				end
				current = current + 2
				goto theEnd
			end
			if (double_metaphone.string_at(original, current, 3, {"SIO", "SIA"}) or double_metaphone.string_at(original, current, 4, {"SIAN"})) then
				if (not double_metaphone.slavo_Germanic(original)) then
					primary = primary .. "S"
					secondary = secondary .. "X"
				else
					primary = primary .. "S"
					secondary = secondary .. "S"
				end
				current = current + 3
				goto theEnd
			end
			if (((current == 1) and double_metaphone.string_at(original, (current + 1), 1, {"M", "N", "L", "W"})) or double_metaphone.string_at(original, (current + 1), 1, {"Z"})) then
				primary = primary .. "S"
				secondary = secondary .. "X"
				if (double_metaphone.string_at(original, (current + 1), 1, {"Z"})) then
					current = current + 2
				else
					current = current + 1
				end
				goto theEnd
			end
			if (double_metaphone.string_at(original, current, 2, {"SC"})) then
				if (original:sub(current + 2, current + 2) == "H") then
					if (double_metaphone.string_at(original, (current + 3), 2, {"OO", "ER", "EN", "UY", "ED", "EM"})) then
						if (double_metaphone.string_at(original, (current + 3), 2, {"ER", "EN"})) then
							primary = primary .. "X"
							secondary = secondary .. "SK"
						else
							primary = primary .. "SK"
							secondary = secondary .. "SK"
						end
						current = current + 3
						goto theEnd
					else
						if ((current == 1) and not double_metaphone.is_vowel(original, 4) and (original:sub(4, 4) ~= "W")) then
							primary = primary .. "X"
							secondary = secondary .. "S"
						else
							primary = primary .. "X"
							secondary = secondary .. "X"
						end
						current = current + 3
						goto theEnd
					end
				end
				if (double_metaphone.string_at(original, (current + 2), 1, {"I", "E", "Y"})) then
					primary = primary .. "S"
					secondary = secondary .. "S"
					current = current + 3
					goto theEnd
				end
				primary = primary .. "SK"
				secondary = secondary .. "SK"
				current = current + 3
				goto theEnd
			end
			if ((current == last) and double_metaphone.string_at(original, (current - 2), 2, {"AI", "OI"})) then
				primary = primary .. ""
				secondary = secondary .. "S"
			else
				primary = primary .. "S"
				secondary = secondary .. "S"
			end
			if (double_metaphone.string_at(original, (current + 1), 1, {"S", "Z"})) then
				current = current + 2
			else
				current = current + 1
			end
			goto theEnd
		elseif original:sub(current, current) == "T" then
			if (double_metaphone.string_at(original, current, 4, {"TION"})) then
				primary = primary .. "X"
				secondary = secondary .. "X"
				current = current + 3
				goto theEnd
			end
			if (double_metaphone.string_at(original, current, 3, {"TIA", "TCH"})) then
				primary = primary .. "X"
				secondary = secondary .. "X"
				current = current + 3
				goto theEnd
			end
			if (double_metaphone.string_at(original, current, 2, {"TH"}) or double_metaphone.string_at(original, current, 3, {"TTH"})) then
				if (double_metaphone.string_at(original, (current + 2), 2, {"OM", "AM"}) or double_metaphone.string_at(original, 1, 4, {"VAN ", "VON "}) or double_metaphone.string_at(original, 1, 3, {"SCH"})) then
					primary = primary .. "T"
					secondary = secondary .. "T"
				else
					primary = primary .. "0"
					secondary = secondary .. "T"
				end
				current = current + 2
				goto theEnd
			end
			if (double_metaphone.string_at(original, (current + 1), 1, {"T", "D"})) then
				current = current + 2
			else
				current = current + 1
			end
			primary = primary .. "T"
			secondary = secondary .. "T"
			goto theEnd
		elseif original:sub(current, current) == "V" then
			if (original:sub(current + 1, current + 1) == "V") then
				current = current + 2
			else
				current = current + 1
			end
			primary = primary .. "F"
			secondary = secondary .. "F"
			goto theEnd
		elseif original:sub(current, current) == "W" then
			if (double_metaphone.string_at(original, current, 2, {"WR"})) then
				primary = primary .. "R"
				secondary = secondary .. "R"
				current = current + 2
				goto theEnd
			end
			if current == 1 and (double_metaphone.is_vowel(original, current + 1) or double_metaphone.string_at(original, current, 2, {"WH"})) then
				if (double_metaphone.is_vowel(original, current + 1)) then
					primary = primary .. "A"
					secondary = secondary .. "F"
				else
					primary = primary .. "A"
					secondary = secondary .. "A"
				end
			end
			if ((current == last and double_metaphone.is_vowel(original, current - 1)) or double_metaphone.string_at(original, (current - 1), 5, {"EWSKI", "EWSKY", "OWSKI", "OWSKY"})
			or double_metaphone.string_at(original, 1, 3, {"SCH"})) then
				primary = primary .. ""
				secondary = secondary .. "F"
				current = current + 1
				goto theEnd
			end
			if (double_metaphone.string_at(original, current, 4, {"WICZ", "WITZ"})) then
				primary = primary .. "TS"
				secondary = secondary .. "FX"
				current = current + 4
				goto theEnd
			end
			current = current + 1
			goto theEnd
		elseif original:sub(current, current) == "X" then
			if (not(current == last and (double_metaphone.string_at(original, (current - 3), 3, {"IAU", "EAU"}) or double_metaphone.string_at(original, (current - 2), 2, {"AU", "OU"})))) then
				primary = primary .. "KS"
				secondary = secondary .. "KS"
			end
			if (double_metaphone.string_at(original, (current + 1), 1, {"C", "X"})) then
				current = current + 2
			else
				current = current + 1
			end
			goto theEnd
		elseif original:sub(current, current) == "Z" then
			if (original:sub(current + 1, current + 1) == "H") then
				primary = primary .. "J"
				secondary = secondary .. "J"
				current = current + 2
				goto theEnd
			elseif (double_metaphone.string_at(original, (current + 1), 2, {"ZO", "ZI", "ZA"}) or (double_metaphone.slavo_Germanic(original) and ((current > 1) and original:sub(current - 1, current - 1) ~= "T"))) then
				primary = primary .. "S"
				secondary = secondary .. "TS"
			else
				primary = primary .. "S"
				secondary = secondary .. "S"
			end
			if (original:sub(current + 1, current + 1) == "Z") then
				current = current + 2
			else
				current = current + 1
			end
			goto theEnd
		end
		if current == oldCurrent then
			current = current + 1
		end
		::theEnd::
    end
	return {primary = primary, secondary = secondary}
end

if arg[1] then
	double_metaphone.erg = double_metaphone.transscript(arg[1])
	print("Primary: " .. double_metaphone.erg.primary)
	if double_metaphone.erg.primary ~= double_metaphone.erg.secondary then
		print("Secondary: " .. double_metaphone.erg.secondary)
	else
		print("Secondary: ----")
	end
else
	print("Please provide a string argument!")
end